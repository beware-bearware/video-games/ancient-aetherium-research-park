# Ancient Aetherium: Research Park

Research Park is a technical project in the Ancient Aetherium series. Research Park uses Godot to provide a testing ground for Captain Clubs, in order to let them get more experience in the engine. Research Park has no direction, and is likely to be full of messy code and considerably unstable as a baseline.

## Installation

Currently the only way to run Research Park is to clone this repository and run it through the Godot Editor. Binaries will be released when Research Park reaches its first stable build.

## Contributing

As well as being new from a technical standpoint, this group itself is also new to providing public and open source projects. Research Park is a testing ground and most input is welcome, however contributors should be aware that the management of Research Park may be messy. Major pull requests should have an issue opened, from our current understanding.
